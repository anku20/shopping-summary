import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { dataItems } from "./ItemsData";

const initialState = {
  items: dataItems
};

const reducer1 = (state = initialState, action) => {
  
  switch (action.type) {
    case "UPDATE":
      // update the state here for the quantity update
      return Object.assign({}, state, {
        ...state,
        items: {
          items: state.items.items.map(item => {
            if (item.style === action.payload.item.style) {
              return Object.assign({}, item, {
                quantity: parseInt(action.payload.qty)
              });
            }
            return item;
          })
        }
      });

    // return newState;
    case "REMOVE":
      return Object.assign({}, state, {
        ...state,
        items: {
          items: state.items.items.filter(
            item => item.style !== action.payload.item.style
          )
        }
      });
    default:
      return state;
  }
  // return state;
};
const store = createStore(reducer1);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
