import React, { Component } from "react";

export default class OrderSummary extends Component {
  getSubTotal = (items) =>{
    let subTotal = 0;
    items.forEach(item => {
      subTotal+= item.quantity * item.price;
    }); 
    return subTotal;
  }
  render() {
    return (
      <section className="jumbotron">
        <div className="row text-center title-style">
          <div className="col-12"><h3>ORDER SUMMARY</h3></div>
        </div>
        <div className="row padding-bottom">
          <div className="col-6">SUBTOTAL</div>
          <div className="col-6 text-right" >£{Number(this.getSubTotal(this.props.items.items)).toFixed(2)}</div>
        </div>
        <div className="row padding-bottom">
          <div className="col-6" >Shipping</div>
          <div className="col-6 text-right" >From FREE</div>
        </div>
        <hr/>
        <div className="row padding-bottom">
          <div className="col-6 boldfont" >TOTAL <br/> <span className="small-font">(Before Shipping)</span></div>
          <div className="col-6 text-right boldfont" >£{Number(this.getSubTotal(this.props.items.items)).toFixed(2)}</div>
        </div>


      </section>
    );
  }
}
