import React, { Component } from "react";
import PropTypes from "prop-types";

export default class ItemDetails extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    // this.state = {
    //   item: this.props.itemDetail
    // }
    this.handleChange = this.handleChange.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
  }
  render() {
    return (
      <section>
        <article className="row">
          <div className="col-3 col-lg-3">
            <img
              className="imgdimension"
              src="https://dummyimage.com/150x150/2ade39/eb4909"
              alt="Grey"
            />
          </div>
          <div className="col-9 col-lg-3">
            <div className="row">
              <div className="col-12 margin-left-10">
                <h3>{this.props.itemDetail.name}</h3>
              </div>
              <div className="col-12">
                <h6>Style # {this.props.itemDetail.style}</h6>
              </div>
            </div>
            <div className="row">
              <div className="col-6 col-lg-3">
                <h6>Color:</h6>
              </div>
              <div className="col-6 col-lg-9 text-right text-align-left">
                <h6>{this.props.itemDetail.Colour}</h6>
              </div>
            </div>
            <div className="row">
              <div className="col-6 col-lg-3">
                <h6>Size:</h6>
              </div>
              <div className="col-6 col-lg-9 text-right text-align-left">
                <h6>{this.props.itemDetail.size}</h6>
              </div>
            </div>
          </div>
          <div className="col-9 col-lg-6  offset-3 offset-lg-0 float-right">
            <div className="row">
              <div className="col-6 show-on-small">
                <h6>Quantity</h6>
              </div>
              <div className="col-6 order-lg-1 col-lg-4">
                <div className="form-group float-right">
                  <select
                    value={this.props.itemDetail.quantity}
                    onChange={
                      this.handleChange
                      // this.props.onDropdownChange(this.props.itemDetail,)
                    }
                    className="form-control custom-select-sm"
                    id="sel1"
                  >
                    <option value={1}>1</option> */}
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                    <option value={4}>4</option>
                  </select>
                </div>
              </div>
              <div className="col-6 show-on-small">
                <h6>Price</h6>
              </div>
              <div className="col-6 col-lg-4 order-lg-0 text-right">
                <h6>£{Number(this.props.itemDetail.price).toFixed(2)}</h6>
              </div>
              <div className="col-6 show-on-small">
                <h4>Subtotal</h4>
              </div>
              <div className="col-6 col-lg-4 order-lg-2  text-right">
                <h4>
                  £
                  {Number(
                    this.props.itemDetail.quantity * this.props.itemDetail.price
                  ).toFixed(2)}
                </h4>
              </div>
            </div>
          </div>
        </article>
        <section className="row negtive-margin">
          <div className="col-12 col-lg-3 order-lg-1 float-right text-right">
            <h5>Edit</h5>
          </div>
          <div className="col-6 col-lg-3 order-lg-0 offset-lg-3">
            <h5>Add to Favorites</h5>
          </div>
          <div className="col-6 col-lg-3 order-lg-2 text-right">
            <h5>
              {" "}
              <button onClick={this.handleRemove}>REMOVE</button>
            </h5>
          </div>
        </section>
        <hr />

        {/* <div className="row">
          <div className="col-4 padding-left ">
            <img
              className="imgdimension"
              src="https://via.placeholder.com/150"
              alt="Grey"
            />
          </div>
          <div className="col-8 margin-left-10">
            <div className="row title-style">
              <div className="col-12 ">
                <h4>{this.props.itemDetail.name}</h4>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                Style # {this.props.itemDetail.style}
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <label>Size:</label>
              </div>
              <div className="col-6 text-right">
                <label>{this.props.itemDetail.size}</label>
              </div>
            </div> */}
        {/* <div className="row">
              <div className="col-6 show-on-small">
                <label>Quantity</label>
              </div>
              <div className="col-6">
                <div className="form-group float-right">
                  <select
                    value={this.props.itemDetail.quantity}
                    onChange={
                      this.handleChange
                      // this.props.onDropdownChange(this.props.itemDetail,)
                    }
                    className="form-control"
                    id="sel1"
                  >
                    <option value={1}>1</option> */}
        {/* <option value={2}>2</option>
                    <option value={3}>3</option>
                    <option value={4}>4</option>
                  </select>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-6 show-on-small">
                <label>Price:</label>
              </div>
              <div className="col-6 text-right">
                <label>£{Number(this.props.itemDetail.price).toFixed(2)}</label>
              </div>
            </div> */}

        {/* <div className="row boldfont">
              <div className="col-6 show-on-small">Subtotal:</div>
              <div className="col-6 text-right">
                £
                {Number(
                  this.props.itemDetail.quantity * this.props.itemDetail.price
                ).toFixed(2)}
              </div>
            </div>
          </div>
        </div>

        <div className="row text-right padding-top">
          <div className="col-12 ">EDIT</div>
        </div>
        <div className="row">
          <div className="col-6 padding-left">ADD TO FAVORITES</div>
          <div className="col-6 text-right">
            <button onClick={this.handleRemove}>REMOVE</button>
          </div>
        </div>
        <hr /> */}
      </section>
    );
  }

  handleChange(event) {
    // this.setState({(state).item.quantity = event.target.value});
    this.props.onDropdownChange(this.props.itemDetail, event.target.value);
  }
  handleRemove() {
    debugger;
    // this.setState({(state).item.quantity = event.target.value});
    this.props.onRemoveItem(this.props.itemDetail);
  }
}
ItemDetails.propTypes = {
  itemDetail: PropTypes.shape({
    name: PropTypes.string,
    size: PropTypes.string,
    Colour: PropTypes.string,
    quantity: PropTypes.number,
    price: PropTypes.number,
    style: PropTypes.string
  })
};
