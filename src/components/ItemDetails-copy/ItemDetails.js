import React, { Component } from "react";

export default class ItemDetails extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    // this.state = {
    //   item: this.props.itemDetail
    // }
    this.handleChange = this.handleChange.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
  }
  render() {
    return (
      <section>
      
        <div className="row">
          <div className="col-4 padding-left ">
            <img
              className="imgdimension"
              src="https://via.placeholder.com/150"
              alt="Grey"
            />
          </div>
          <div className="col-8 margin-left-10">
            <div className="row title-style">
              <div className="col-12 ">
                <h4>{this.props.itemDetail.name}</h4>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                Style # {this.props.itemDetail.style}
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <label>Size:</label>
              </div>
              <div className="col-6 text-right">
                <label>{this.props.itemDetail.size}</label>
              </div>
            </div>
            <div className="row">
              <div className="col-6 show-on-small">
                <label>Quantity</label>
              </div>
              <div className="col-6">
                <div className="form-group float-right">
                  <select
                    value={this.props.itemDetail.quantity}
                    onChange={
                      this.handleChange
                      // this.props.onDropdownChange(this.props.itemDetail,)
                    }
                    className="form-control"
                    id="sel1"
                  >
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                    <option value={4}>4</option>
                  </select>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-6 show-on-small">
                <label>Price:</label>
              </div>
              <div className="col-6 text-right">
                <label>£{Number(this.props.itemDetail.price).toFixed(2)}</label>
              </div>
            </div>

            <div className="row boldfont">
              <div className="col-6 show-on-small">Subtotal:</div>
              <div className="col-6 text-right">
                £
                {Number(
                  this.props.itemDetail.quantity * this.props.itemDetail.price
                ).toFixed(2)}
              </div>
            </div>
          </div>
        </div>

        <div className="row text-right padding-top">
          <div className="col-12 ">EDIT</div>
        </div>
        <div className="row">
          <div className="col-6 padding-left">ADD TO FAVORITES</div>
          <div className="col-6 text-right">
            <button onClick={this.handleRemove}>REMOVE</button>
          </div>
        </div>
        <hr />
      </section>
    );
  }

  handleChange(event) {
    // this.setState({(state).item.quantity = event.target.value});
    this.props.onDropdownChange(this.props.itemDetail, event.target.value);
  }
  handleRemove() {
    debugger;
    // this.setState({(state).item.quantity = event.target.value});
    this.props.onRemoveItem(this.props.itemDetail);
  }
}
