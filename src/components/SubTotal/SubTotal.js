import React, { Component } from 'react'

export default class SubTotal extends Component {

  getSubTotal = (items) =>{
    let subTotal = 0;
    items.forEach(item => {
      subTotal+= item.quantity * item.price;
    }); 
    return subTotal;
  }

  getTotalQty = (items) =>{
    let subqty = 0;
    items.forEach(item => {
      subqty+= item.quantity;
    }); 
    return subqty;
  }
  render() {
    const items = this.props.items.items;
    const subTotal = this.getSubTotal(items);
    const subQty = this.getTotalQty(items);

    return (
      <div className="row">
      <div className="col-12 col-lg-6 adjust-text-left">
      <h3>YOUR BAG({subQty})</h3>
      </div>
      <div className="col-12 col-lg-6  adjust-text-right">
      <h5>SUBTOTAL £{Number(subTotal).toFixed(2) }</h5>
      </div>
       
      </div>
    )
  }
}