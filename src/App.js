import React, { Component } from "react";
import "./App.css";
import SubTotal from "./components/SubTotal/SubTotal";
import ItemDetails from "./components/ItemDetails/ItemDetails";
// import PromoCode from "./components/PromoCode/PromoCode";
import OrderSummary from "./components/OrderSummary/OrderSummary";
// import { dataItems } from "./ItemsData";
import { connect } from "react-redux";


class App extends Component {
  constructor(props) {
    super(props);
    // this.state = dataItems;
    this.updateState = this.updateState.bind(this);
  }

  render() {
    // const subTotal = this.getSubTotal(this.state.items);
    return (
      <div className="container-fluid">
        <div className="row d-flex d-lg-block">
          <div className="col-12 order-0 col-lg-8 order-lg-0 float-left">
            <section className="row justify-content-center center-block text-center">
              <div className="col-12">
                <SubTotal items={this.props.items} />
              </div>
            </section>

            <hr />
          </div>
          <div className="show-on-large col-12 order-0 float-left col-lg-8 order-lg-0">
            <div className="row">
              <div className="col-lg-6">Product Description</div>
              <div className="col-lg-2 text-right">Price</div>
              <div className="col-lg-2 text-right">Quantity</div>
              <div className="col-lg-2 text-right">Subtotal</div>
            </div>
            <hr/>
          </div>

          <div className="col-12 order-0 col-lg-8 order-lg-0 float-left">
            <section className="row justify-content-center center-block text-cente">
              <div className="col-12">
                {this.props.items.items.map((item, index) => (
                  <ItemDetails
                    onRemoveItem={this.updateState}
                    onDropdownChange={this.updateState}
                    key={index}
                    itemDetail={item}
                  />
                ))}
              </div>
            </section>
          </div>
          <div className="col-12 order-1 col-lg-4 order-lg-1 float-right">
            <section className="row">
              <div className="col-12">
                <OrderSummary items={this.props.items} />
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }

  updateState = (itemSelected, qty) => {
    // dispatch the actions
    if (qty) {
      this.props.dispatch({
        type: "UPDATE",
        payload: { item: itemSelected, qty: qty }
      });
      // qty = parseInt(qty);
      // this.setState(state => ({
      //   items: state.items.map(item => {
      //     debugger;

      //     if (item.style === itemSelected.style) {
      //       item.quantity = qty;
      //     }
      //     return item;
      //   })
      // }));
    } else {
      // this.setState(state => ({
      //   items: state.items.filter(item => item.style !== itemSelected.style
      //   )
      // }));

      this.props.dispatch({ type: "REMOVE", payload: { item: itemSelected } });
    }

    // console.log(item);

    // const selectedItem =  this.state.items.filter(item => item.style === itemSelected.style);
  };
}
const mapStateToProps = state => ({
  items: state.items
});


export default connect(mapStateToProps)(App);
