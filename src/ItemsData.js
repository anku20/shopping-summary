export const dataItems = {
  items: [
    {
      name: "Varsity Jacket",
      size: "S",
      Colour: "Green",
      quantity: 1,
      price: 275,
      style: "ABCDEFGHI"
    },
    {
      name: "ABC Jacket",
      size: "M",
      Colour: "Red",
      quantity: 2,
      price: 300,
      style: "FGHIJKLM"
    }
  ]
};

